﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.DataModel;

namespace WindowsFormsApplication1
{
    public partial class frmNewPhonebook : Form
    {
        private bool AddNew { get { return MainData == null; } }
        public DataModel.dm_phonebook MainData { get; set; }

        public frmNewPhonebook(DataModel.dm_phonebook data = null)
        {
            InitializeComponent();

            MainData = data;
            if (AddNew)
                dbs_data.DataSource = new DataModel.dm_phonebook();
            else
                dbs_data.DataSource = copyData(data);
            dbs_data.ResetBindings(true);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MainData = null;
            this.Close();
        }

        private void button_save_Click(object sender, EventArgs e)
        {
            var data = (DataModel.dm_phonebook)dbs_data.DataSource;
            //policy????

            MainData = copyData(data);
            this.Close();
        }

        private dm_phonebook copyData(dm_phonebook data)
        {
            return new DataModel.dm_phonebook()
            {
                address = data.address,
                codeMeli = data.codeMeli,
                email = data.email,
                family = data.family,
                id = data.id,
                mobile = data.mobile,
                name = data.name,
                tel = data.tel
            };
        }
    }
}
