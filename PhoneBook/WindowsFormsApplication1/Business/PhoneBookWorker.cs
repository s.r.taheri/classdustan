﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DataModel;

namespace WindowsFormsApplication1.Business
{
    public class PhoneBookWorker
    {
        public enum typeSearch
        {
            name = 0,
            family = 1,
            mobile = 2
        }

        internal static void add(dm_phonebook mainData)
        {
            try
            {
                ConsoleApplication1.srtTextFile file = new ConsoleApplication1.srtTextFile
                    (Environment.CurrentDirectory, "saeed", Encoding.UTF8);

                List<string> lstData = new List<string>();
                lstData.Add(mainData.ToString());
                file.WriteData(lstData);
            }
            catch (Exception exp)
            {
                throw new Exception("Error For Saving", exp);
            }
        }

        internal static List<dm_phonebook> getAllData()
        {
            try
            {
                var file = new ConsoleApplication1.srtTextFileGeneric<dm_phonebook>
                    (Environment.CurrentDirectory, "saeed", Encoding.UTF8);

                var lstData = file.ReadData();
                return lstData;
            }
            catch (Exception exp)
            {
                throw new Exception("Error For Loading", exp);
            }
        }

        internal static void update(dm_phonebook mainData, dm_phonebook oldData, List<dm_phonebook> lstoldData)
        {
            try
            {
                //var lstoldData = getAllData();
                lstoldData.RemoveAll(q => q.name == oldData.name && q.family == oldData.family);
                lstoldData.Add(mainData);

                ConsoleApplication1.srtTextFile file = new ConsoleApplication1.srtTextFile
                    (Environment.CurrentDirectory, "saeed", Encoding.UTF8);

                List<string> lstData = new List<string>();
                foreach (var item in lstoldData)
                {
                    lstData.Add(item.ToString());
                }

                //lstoldData.ForEach(q => lstData.Add(q.ToString()));

                file.delete();
                file.WriteData(lstData);
            }
            catch (Exception exp)
            {
                throw new Exception("Error For Saving", exp);
            }
        }

        internal static List<dm_phonebook> search(string text, typeSearch type_Search)
        {
            try
            {
                var lstRet = new List<dm_phonebook>();

                var lstData = getAllData();

                switch (type_Search)
                {
                    case typeSearch.name:
                        lstRet = lstData.Where(q => q.name.IndexOf(text) >= 0).ToList();
                        break;
                    case typeSearch.family:
                        lstRet = lstData.Where(q => q.family.IndexOf(text) >= 0).ToList();
                        break;
                    case typeSearch.mobile:
                        lstRet = lstData.Where(q => q.mobile.ToString().IndexOf(text) >= 0).ToList();
                        break;
                    default:
                        break;
                }

                return lstRet;
            }
            catch { return new List<dm_phonebook>(); }
        }

        internal static void delete(dm_phonebook dm)
        {
            try
            {
                //using (var file = new ConsoleApplication1.srtTextFile("d:\\", "saeed", Encoding.UTF8))
                //{
                //    var lstData = file.ReadData().Select(q => string.IsNullOrEmpty(q) ? new dm_phonebook() : convertData(q)).ToList();

                //    lstData.RemoveAll(q => string.IsNullOrEmpty(q.name));

                //    lstData.RemoveAll(q => dm.name == q.name && dm.family == q.family);

                //    file.delete();
                //    file.WriteData(lstData.Select(q => q.ToString()).ToList());
                //}

            }
            catch (Exception exp)
            {
                throw new Exception("Error For delete", exp);
            }
        }
    }
}
