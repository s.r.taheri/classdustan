﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();

            resetPage();
        }

        private void resetPage()
        {
            List<DataModel.dm_phonebook> lst;
            try
            {
                lst = Business.PhoneBookWorker.getAllData();
            }
            catch { lst = new List<DataModel.dm_phonebook>(); }

            dbs_main.DataSource = lst;
            dbs_main.ResetBindings(true);
        }

        private void button_AddNew_Click(object sender, EventArgs e)
        {
            try
            {
                using (frmNewPhonebook frm = new frmNewPhonebook())
                {
                    frm.ShowDialog();
                    if (frm.MainData == null) return;

                    Business.PhoneBookWorker.add(frm.MainData);
                    dbs_main.Add(frm.MainData);
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show("Error For Save new" + exp.Message);
            }
        }

        private void dataGridView1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(dbs_main.Position.ToString());
            //MessageBox.Show(dbs_main.Current.ToString());
        }

        private void button_edit_Click(object sender, EventArgs e)
        {
            if (dbs_main.Count == 0) return;
            try
            {
                var dm = (DataModel.dm_phonebook)dbs_main.Current;
                using (frmNewPhonebook frm = new frmNewPhonebook(dm))
                {
                    frm.ShowDialog();
                    if (frm.MainData == null) return;

                    Business.PhoneBookWorker.update(frm.MainData, dm,
                        (List<DataModel.dm_phonebook>)dbs_main.DataSource);
                    dbs_main[dbs_main.Position] = frm.MainData;
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show("Error For edit Data" + exp.Message);
            }
        }

        private void button_Search_Click(object sender, EventArgs e)
        {
            if (txt_search.Text == "")
            {
                resetPage();
                return;
            }

            var typeSearch = Business.PhoneBookWorker.typeSearch.name;
            if (op_family.Checked) typeSearch = Business.PhoneBookWorker.typeSearch.family;
            else if (op_mobile.Checked) typeSearch = Business.PhoneBookWorker.typeSearch.mobile;

            List<DataModel.dm_phonebook> lstData = Business.PhoneBookWorker.search(txt_search.Text, typeSearch);

            dbs_main.DataSource = lstData;
            dbs_main.ResetBindings(true);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dbs_main.Count == 0) return;
            try
            {
                var dm = (DataModel.dm_phonebook)dbs_main.Current;
                Business.PhoneBookWorker.delete(dm);

                resetPage();
            }
            catch (Exception exp)
            {
                MessageBox.Show("Error For delete Data" + exp.Message);
            }
        }

        private void btn_reset_Click(object sender, EventArgs e)
        {
            resetPage();
        }

        private void txt_search_TextChanged(object sender, EventArgs e)
        {
            if (txt_search.Text == "")
                resetPage();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            WindowsFormsApplication2.DataModel.childClass cc = new WindowsFormsApplication2.DataModel.childClass();
        }
    }
}
