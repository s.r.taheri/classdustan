﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DataModel
{
    public class dm_phonebook : ConsoleApplication1.SaeedInterface
    {
        public int id { get; set; }
        public string name { get; set; }
        public string family { get; set; }
        public long mobile { get; set; }
        public string address { get; set; }
        public long tel { get; set; }
        public string email { get; set; }
        public string codeMeli { get; set; }

        public override string ToString()
        {
            return id + "," + name + "," + family + "," + mobile + "," + address + "," + tel + "," + email + "," + codeMeli;
        }

        public void readFromString(string data)
        {
            var d = data.Split(',');

            int i = 0;
            id = int.Parse(d[i++]);
            name = d[i++];
            family = d[i++];
            mobile = long.Parse(d[i++]);
            address = d[i++];
            tel = long.Parse(d[i++]);
            email = d[i++];
            codeMeli = d[i++];
        }
    }
}
