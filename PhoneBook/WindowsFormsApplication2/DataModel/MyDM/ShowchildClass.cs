﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication2.DataModel.MyDM
{
    class ShowchildClass
    {
        childClass MainData;
        public ShowchildClass(childClass cc)
        {
            MainData = cc;
        }
        public string name { get { return MainData.name; } }
        public DateTime date { get { return MainData.date; } }
        public string date_Persian
        {
            get
            {
                PersianCalendar pc = new PersianCalendar();
                int y = pc.GetYear(date);
                int m = pc.GetMonth(date);
                int d = pc.GetDayOfMonth(date);

                return y.ToString() + " - " + m.ToString() + " - " + d.ToString();
            }
        }

    }
}
