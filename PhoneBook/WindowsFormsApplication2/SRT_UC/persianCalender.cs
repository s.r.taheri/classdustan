﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2.SRT_UC
{
    public partial class persianCalender : UserControl
    {
        public persianCalender()
        {
            InitializeComponent();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            DataModel.childClass cc = new DataModel.childClass()
            {
                date = dateTimePicker1.Value,
                name = "saeed"
            };

            DataModel.MyDM.ShowchildClass scc = new DataModel.MyDM.ShowchildClass(cc);
            label1.Text = scc.date_Persian;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dateTimePicker1.Value = dateTimePicker1.Value.AddDays(1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            dateTimePicker1.Value = dateTimePicker1.Value.AddDays(-1);
        }
    }
}
