﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2.SRT_UC
{
    public partial class MainUC : Button
    {
        SRTColor _MyColor;
        public SRTColor MyColor
        {
            get { return _MyColor; }
            set
            {
                _MyColor = value;
                setColorBase();
            }
        }

        public override Font Font
        {
            get
            {
                return base.Font;
            }

            set
            {
                if (value.Size > 12) throw new Exception("font Out of size");
                base.Font = value;
            }
        }

        void setColorMouseEnter()
        {
            switch (MyColor)
            {
                case SRTColor.Red:
                    changeBackColor(Color.Orange);
                    break;
                case SRTColor.yellow:
                    changeBackColor(Color.GreenYellow);
                    break;
                case SRTColor.blue:
                    changeBackColor(Color.Aqua);
                    break;
                default:
                    throw new Exception();
            }
        }
        void setColorBase()
        {
            switch (MyColor)
            {
                case SRTColor.Red:
                    changeBackColor(Color.Red);
                    base.Image = Properties.Resources.logo;
                    break;
                case SRTColor.yellow:
                    changeBackColor(Color.Yellow);
                    base.Image = Properties.Resources.icons8_AliExpress_64;
                    break;
                case SRTColor.blue:
                    changeBackColor(Color.Blue);
                    base.Image = Properties.Resources.icons8_AliExpress_128;
                    break;
                default:
                    throw new Exception();
            }
        }
        void setColorMouseClick()
        {
            switch (MyColor)
            {
                case SRTColor.Red:
                    changeBackColor(Color.DarkRed);
                    break;
                case SRTColor.yellow:
                    changeBackColor(Color.Gold);
                    break;
                case SRTColor.blue:
                    changeBackColor(Color.DarkBlue);
                    break;
                default:
                    throw new Exception();
            }
        }


        public enum SRTColor
        {
            Red, yellow, blue
        }
        public MainUC()
        {
            InitializeComponent();

            setColorBase();
        }

        void changeBackColor(Color color)
        {
            this.BackColor = color;
        }
        private void MainUC_MouseEnter(object sender, EventArgs e)
        {
            setColorMouseEnter();
        }

        private void MainUC_MouseLeave(object sender, EventArgs e)
        {
            setColorBase();
        }

        private void MainUC_MouseClick(object sender, MouseEventArgs e)
        {
            setColorMouseClick();
        }
    }
}
