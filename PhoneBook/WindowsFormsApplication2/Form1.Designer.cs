﻿namespace WindowsFormsApplication2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.mainUC1 = new WindowsFormsApplication2.SRT_UC.MainUC();
            this.mainUC2 = new WindowsFormsApplication2.SRT_UC.MainUC();
            this.persianCalender1 = new WindowsFormsApplication2.SRT_UC.persianCalender();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(44, 12);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 1, 4, 1);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(156, 26);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // mainUC1
            // 
            this.mainUC1.BackColor = System.Drawing.Color.Red;
            this.mainUC1.Image = ((System.Drawing.Image)(resources.GetObject("mainUC1.Image")));
            this.mainUC1.Location = new System.Drawing.Point(518, 92);
            this.mainUC1.Margin = new System.Windows.Forms.Padding(4, 1, 4, 1);
            this.mainUC1.MyColor = WindowsFormsApplication2.SRT_UC.MainUC.SRTColor.Red;
            this.mainUC1.Name = "mainUC1";
            this.mainUC1.Size = new System.Drawing.Size(151, 33);
            this.mainUC1.TabIndex = 2;
            this.mainUC1.Text = "mainUC1";
            this.mainUC1.UseVisualStyleBackColor = false;
            // 
            // mainUC2
            // 
            this.mainUC2.BackColor = System.Drawing.Color.Red;
            this.mainUC2.Image = ((System.Drawing.Image)(resources.GetObject("mainUC2.Image")));
            this.mainUC2.Location = new System.Drawing.Point(340, 109);
            this.mainUC2.Margin = new System.Windows.Forms.Padding(4, 1, 4, 1);
            this.mainUC2.MyColor = WindowsFormsApplication2.SRT_UC.MainUC.SRTColor.Red;
            this.mainUC2.Name = "mainUC2";
            this.mainUC2.Size = new System.Drawing.Size(128, 29);
            this.mainUC2.TabIndex = 3;
            this.mainUC2.Text = "mainUC2";
            this.mainUC2.UseVisualStyleBackColor = false;
            // 
            // persianCalender1
            // 
            this.persianCalender1.Location = new System.Drawing.Point(23, 61);
            this.persianCalender1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.persianCalender1.Name = "persianCalender1";
            this.persianCalender1.Size = new System.Drawing.Size(229, 64);
            this.persianCalender1.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(828, 168);
            this.Controls.Add(this.persianCalender1);
            this.Controls.Add(this.mainUC2);
            this.Controls.Add(this.mainUC1);
            this.Controls.Add(this.button1);
            this.Margin = new System.Windows.Forms.Padding(2, 8, 2, 8);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private SRT_UC.MainUC mainUC1;
        private SRT_UC.MainUC mainUC2;
        private SRT_UC.persianCalender persianCalender1;
    }
}

