﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    static class myclass
    {
        public static string toStringNumber(this string number, string separete = ",")
        {
            var lstData = Mysplit(number, 3, true);
            var ret = lstData.First();

            for (int i = 1; i < lstData.Count; i++)
            {
                ret += separete + lstData[i];
            }

            return ret;
        }

        private static List<string> Mysplit(string str, int chunkSize, bool fromEnd = false)
        {
            var lstRet = new List<string>();
            if (fromEnd)
            {
                var p0 = str.Length % chunkSize;
                if (p0 != 0)
                {
                    lstRet.Add(str.Substring(0, p0));
                    str = new string(str.Skip(p0).ToArray());
                }
            }
            while (!string.IsNullOrEmpty(str))
            {
                var d = new string(str.Take(chunkSize).ToArray());
                lstRet.Add(d);
                str = new string(str.Skip(chunkSize).ToArray());
            }
            return lstRet;
        }
    }
}
