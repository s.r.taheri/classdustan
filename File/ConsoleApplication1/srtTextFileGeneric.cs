﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    /// <summary>
    /// class for working with text file
    /// </summary>
    public class srtTextFileGeneric<tModel> : IDisposable
        where tModel : SaeedInterface, new()
    {
        private string address = "";
        private string nameFile = "";
        public string path { get { return address + nameFile; } }
        private Encoding encode;
        public bool Exist { get { return File.Exists(path); } }
        public srtTextFileGeneric(string _address, string _name, Encoding _encode)
        {
            address = checkAddress(_address);
            nameFile = checkNmae(_name);
            encode = _encode;
        }

        private string checkAddress(string value)
        {
            if (value.Last().ToString() == @"\")
                return value;
            return value + @"\";
        }
        private string checkNmae(string value)
        {
            if (value.IndexOf(".txt") > 0)
                return value;
            return value + ".txt";
        }

        ~srtTextFileGeneric()
        {
            Dispose();
        }
        public void Dispose()
        {

        }

        /// <summary>
        /// Read Data From File
        /// </summary>
        /// <returns>return all line of data</returns>
        public List<tModel> ReadData()
        {
            try
            {
                List<tModel> _ret = new List<tModel>();

                using (var fs = new StreamReader(address + nameFile, encode))
                {
                    while (fs.Peek() != -1)
                    {
                        var d = fs.ReadLine();
                        if (string.IsNullOrEmpty(d)) continue;
                        var dm = new tModel();
                        dm.readFromString(d);
                        _ret.Add(dm);
                    }

                    fs.Close();
                    //fs.Dispose();
                }


                return _ret;
            }
            catch (Exception exp)
            { throw new Exception("Can Not Read File => " + nameFile, exp); }
        }//end read data

        /// <summary>
        /// save data to file
        /// </summary>
        /// <param name="lst_data">data for saving</param>
        public void WriteData(List<tModel> lst_data)
        {
            try
            {
                string data = "";
                foreach (var item in lst_data)
                {
                    data = data + item.ToString() + Environment.NewLine;
                }

                using (var writer =
                    new StreamWriter(address + nameFile, true, encode))
                {
                    // Add some information to the file.
                    writer.WriteLine(data);

                    writer.Close();
                    //writer.Dispose();
                }
            }
            catch (Exception exp)
            { throw new Exception("Can Not Write To File => " + nameFile, exp); }
        }//end write data
    }//end class
}//end namespace
