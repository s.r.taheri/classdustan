﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhoneBook
{
    public partial class frmMain : Form
    {
        public int pageNumber { get { return 1; } }
        public int pageCount { get { return 10; } }
        public class show_tel
        {
            private Data.DataModel.Person_Tel MainData;
            public show_tel(Data.DataModel.Person_Tel data)
            {
                MainData = data;
            }

            public int id { get { return MainData.id; } }

            public string tel { get { return MainData.tel; } }

            public Data.Business.TelType type { get { return (Data.Business.TelType)MainData.type; } }
        }
        public frmMain()
        {
            InitializeComponent();
            ShowData();
        }

        private void ShowData(bool reset = true)
        {
            if (reset)
                dbs_person.DataSource = new Data.Business.Worker.Person_Bus()
                    .GetList(pageNumber, pageCount);
            else
                dbs_person.DataSource = new Data.Business.Worker.Person_Bus()
                    .GetList(q => q != null, pageNumber, pageCount);

            dbs_person.ResetBindings(true);
        }

        private void dbs_person_CurrentChanged(object sender, EventArgs e)
        {
            var dm_person = (Data.DataModel.Person)dbs_person.Current;
            dbs_address.DataSource = new Data.Business.Worker.Person_Address_Bus()
                    .GetList(q => q.id_person == dm_person.id, pageNumber, pageCount);
            dbs_address.ResetBindings(true);
            dbs_tel.DataSource = new Data.Business.Worker.Person_Tel_Bus()
                    .GetList(q => q.id_person == dm_person.id, pageNumber, pageCount);
            dbs_tel.ResetBindings(true);
        }

        private void btn_person_remove_Click(object sender, EventArgs e)
        {
            var dm_person = (Data.DataModel.Person)dbs_person.Current;
            new Data.Business.Worker.Person_Bus().Remove(dm_person);

            dbs_person.RemoveCurrent();
        }
    }
}
