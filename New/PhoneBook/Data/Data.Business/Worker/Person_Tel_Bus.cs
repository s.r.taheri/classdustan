﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Business.Worker
{
    public class Person_Tel_Bus : MyInterface.Worker_Interface<DataModel.Person_Tel>, IDisposable
    {
        private DataAccess.MyRepository.Person_Tel_Repo worker = new DataAccess.MyRepository.Person_Tel_Repo();
        public bool Add(DataModel.Person_Tel data)
        {
            try
            {
                worker.Add(data);
                return worker.Save();
            }
            catch (Exception exp)
            {
                throw new Exception("Worker Person Add", exp);
            }
        }

        public void Dispose()
        {
            try { worker.Dispose(); } catch { }
        }

        public DataModel.Person_Tel GetByID(DataModel.Person_Tel data)
        {
            try
            {
                return worker.GetById(data.id);
            }
            catch (Exception exp)
            {
                throw new Exception("Worker Person Add", exp);
            }
        }

        public List<DataModel.Person_Tel> GetList
            (Expression<Func<DataModel.Person_Tel, bool>> query, int page = 1, int count = 10)
        {
            try
            {
                return worker.FindAll(query)
                             .Skip((page - 1) * count)
                             .Take(count)
                             .ToList();
            }
            catch (Exception exp)
            {
                throw new Exception("Worker Person Add", exp);
            }
        }

        public bool Remove(DataModel.Person_Tel data)
        {
            try
            {
                worker.Remove(data);
                return worker.Save();
            }
            catch (Exception exp)
            {
                throw new Exception("Worker Person Remove", exp);
            }
        }

        public bool Update(DataModel.Person_Tel data)
        {
            try
            {
                worker.Update(data);
                return worker.Save();
            }
            catch (Exception exp)
            {
                throw new Exception("Worker Person Update", exp);
            }
        }
    }
}
