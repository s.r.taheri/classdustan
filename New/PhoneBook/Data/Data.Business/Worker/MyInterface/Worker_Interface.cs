﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Business.Worker.MyInterface
{
    public interface Worker_Interface<TModel>
    {
        bool Add(TModel data);
        bool Remove(TModel data);
        bool Update(TModel data);
        TModel GetByID(TModel data);
        List<TModel> GetList(Expression<Func<TModel, bool>> query, int page = 1, int count = 10);
    }
}
