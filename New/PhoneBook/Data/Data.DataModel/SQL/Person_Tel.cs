namespace Data.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Person_Tel
    {
        public int id { get; set; }

        public int id_person { get; set; }

        [Required]
        [StringLength(50)]
        public string tel { get; set; }

        public byte type { get; set; }

        public virtual Person Person { get; set; }
    }
}
