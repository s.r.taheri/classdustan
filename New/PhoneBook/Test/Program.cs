﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            var lstData = new List<int>() { 1, 3, 5, 7, 9 };
            var func = 10;

            var r1 = lstData.Select(q => q * func);
            var r2 = lstData.Select(q => q * func).ToList();
            func = 100;

            Console.WriteLine("R1");
            foreach (var item in r1)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("R2");
            foreach (var item in r2)
            {
                Console.WriteLine(item);
            }
            func = 1000;

            Console.WriteLine("R1-----------");
            foreach (var item in r1)
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();
        }
    }
}
